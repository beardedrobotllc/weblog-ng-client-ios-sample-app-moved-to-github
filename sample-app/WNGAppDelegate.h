//
//  WNGAppDelegate.h
//  sample-app
//
//  Created by Stephen Kuenzli on 12/28/13.
//  Copyright (c) 2013 Weblog-NG. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WNGAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

- (IBAction) someIntensiveLogic;

@end
